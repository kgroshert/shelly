#!/usr/bin/env python3
# -*- encoding: utf-8; py-indent-offset: 4 -*-
#
#  ____  _          _ _       
# / ___|| |__   ___| | |_   _ 
# \___ \| '_ \ / _ \ | | | | |
#  ___) | | | |  __/ | | |_| |
# |____/|_| |_|\___|_|_|\__, |
#                      |___/ 
#
# created: 01/2022
#
# Author: Frank Baier
# E-Mail: dev@baier-nt.de
#
from typing import Any, Dict, List, Optional

from cmk.utils import aws_constants

import cmk.gui.bi as bi
import cmk.gui.watolib as watolib
from cmk.gui.exceptions import MKUserError
from cmk.gui.i18n import _
from cmk.gui.plugins.metrics.utils import MetricName
from cmk.gui.plugins.wato import (
    HostRulespec,
    IndividualOrStoredPassword,
    monitoring_macro_help,
    rulespec_group_registry,
    rulespec_registry,
    RulespecGroup,
    RulespecSubGroup,
)
from cmk.gui.plugins.wato.utils import PasswordFromStore
from cmk.gui.valuespec import (
    Age,
    Alternative,
    CascadingDropdown,
    Checkbox,
    Dictionary,
    DropdownChoice,
    FixedValue,
    Float,
    Hostname,
    HTTPUrl,
    ID,
    Integer,
    ListChoice,
    ListOf,
    ListOfStrings,
    MonitoringState,
    Password,
    RegExp,
    RegExpUnicode,
    TextAscii,
    TextUnicode,
    Transform,
    Tuple,
)
from cmk.gui.plugins.wato.datasource_programs import RulespecGroupDatasourceProgramsHardware


def _factory_default_special_agents_shelly():
    # No default, do not use setting if no rule matches
    return watolib.Rulespec.FACTORY_DEFAULT_UNUSED


def _valuespec_special_agents_shelly():
    return Dictionary(
        title=_("Shelly Devices"),
        help=_("This rule selects the Sheely agent instead of the "
               "Check_MK Agent and allows monitoring of the Sheely "
               "devices using its HTTP API. "
               "You can configure your connection settings here."),
        elements=[
            ("username", TextAscii(
                title=_("Username"),
                allow_empty=False,
                )
            ),
            ("password", Password(
                title=_("Password"),
                allow_empty=False,
                )
            ),
            ("no_cert_check", FixedValue(
                True,
                title=_("Disable SSL certificate validation"),
                totext=_("SSL certificate validation is disabled"),
                )
            )],
        optional_keys=["no_cert_check"],
    )


rulespec_registry.register(
    HostRulespec(
        factory_default=_factory_default_special_agents_shelly(),
        group=RulespecGroupDatasourceProgramsHardware,
        name="special_agents:shelly",
        valuespec=_valuespec_special_agents_shelly,
    ))


