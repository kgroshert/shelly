#!/usr/bin/env python3
# -*- encoding: utf-8; py-indent-offset: 4 -*-
#
#  ____  _          _ _       
# / ___|| |__   ___| | |_   _ 
# \___ \| '_ \ / _ \ | | | | |
#  ___) | | | |  __/ | | |_| |
# |____/|_| |_|\___|_|_|\__, |
#                      |___/ 
#
# created: 01/2022
#
# Author: Frank Baier
# E-Mail: dev@baier-nt.de
#
from cmk.gui.i18n import _
from cmk.gui.valuespec import (
        Dictionary,
        Integer,
        Tuple,
        TextAscii,
        )
from cmk.gui.plugins.wato import (
        CheckParameterRulespecWithItem,
        CheckParameterRulespecWithoutItem,
        rulespec_registry,
        RulespecGroupCheckParametersEnvironment,
        )

def _parameter_valuespec_shelly_power():
    return Dictionary (
            title = _("SHELLY EM power"),
            optional_keys = ["power_level_lower", "power_level_upper", "current_level_lower", "current_level_upper", "voltage_level_lower", "voltage_level_upper"],
            elements = [
                ("power_level_lower", Tuple (
                    title = _("Lower Power level"),
                    elements = [
                        Float(title=_("Warning below"), size=10, default_value = None, unit="W"),
                        Float(title=_("Critical below"), size=10, default_value = None, unit="W"),
                    ],
                    )),
                ("power_level_upper", Tuple (
                    title = _("Upper Power level"),
                    elements = [
                        Float(title=_("Warning at or above"), size=10, default_value = 3450.0, unit="W"),
                        Float(title=_("Critical at or above"), size=10, default_value = 3680.0, unit="W"),
                    ],
                    )),


                ("current_level_lower", Tuple (
                    title = _("Lower Current level"),
                    elements = [
                        Float(title=_("Warning below"), size=10, default_value = None, unit="A"),
                        Float(title=_("Critical below"), size=10, default_value = None, unit="A"),
                    ],
                    )),
                ("current_level_upper", Tuple (
                    title = _("Upper Current level"),
                    elements = [
                        Float(title=_("Warning at or above"), size=10, default_value = 15.0, unit="A"),
                        Float(title=_("Critical at or above"), size=10, default_value = 16.0, unit="A"),
                    ],
                    )),


                ("voltage_level_lower", Tuple (
                    title = _("Lower Voltage level"),
                    elements = [
                        Float(title=_("Warning below"), size=10, default_value = 210.0, unit="V"),
                        Float(title=_("Critical below"), size=10, default_value = 207.0, unit="V"),
                    ],
                    )),
                ("voltage_level_upper", Tuple (
                    title = _("Upper Voltage level"),
                    elements = [
                        Float(title=_("Warning at or above"), size=10, default_value = 250.0, unit="V"),
                        Float(title=_("Critical at or above"), size=10, default_value = 253.0, unit="V"),
                    ],
                    )),


                ],

            )

rulespec_registry.register (
        CheckParameterRulespecWithoutItem(
            check_group_name = "shelly_power",
            group = RulespecGroupCheckParametersEnvironment,
            match_type = "dict",
            parameter_valuespec = _parameter_valuespec_shelly_power,
            title = lambda: _("SHELLY EM power"),
            )
        )

