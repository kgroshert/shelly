#!/usr/bin/env python3
# -*- encoding: utf-8; py-indent-offset: 4 -*-
#
#  ____  _          _ _       
# / ___|| |__   ___| | |_   _ 
# \___ \| '_ \ / _ \ | | | | |
#  ___) | | | |  __/ | | |_| |
# |____/|_| |_|\___|_|_|\__, |
#                      |___/ 
#
# created: 01/2022
#
# Author: Frank Baier
# E-Mail: dev@baier-nt.de
#
from cmk.gui.i18n import _
from cmk.base.check_api import get_age_human_readable
from .utils import uptime
import json

from .agent_based_api.v1 import (
    all_of,
    check_levels,
    exists,
    not_exists,
    register,
    Service,
    Result,
    State,
    Metric,
) 

from typing import (
    Any,
    Dict,
    Mapping,
    MutableMapping,
    Optional,
    Sequence,
    Tuple,
)

def parse_shelly(string_table):
    """
    :param info: dictionary with all data for all checks and subchecks
    :return:
    """
    if string_table:
        return json.loads(str(string_table[0][0]))
    return dict()


def discover_shelly(section_shelly_settings, section_shelly_status):
    if isinstance(section_shelly_settings.get('device'), dict):
        #yield Service(item=section_shelly_settings.get('device').get('type', None))
        yield Service(item="Device")
    if section_shelly_settings.get('wifi_ap', {}).get('enabled', None):
        yield Service(item="WiFi")
    elif section_shelly_settings.get('wifi_sta', {}).get('enabled', None):
        yield Service(item='WiFi')
    if isinstance(section_shelly_settings.get('mqtt'), dict):
        yield Service(item="MQTT")
    if isinstance(section_shelly_settings.get('coiot'), dict):
        yield Service(item="CoIoT")
    if isinstance(section_shelly_settings.get('cloud'), dict):
        yield Service(item="Cloud")
    if isinstance(section_shelly_status.get('update'), dict):
        yield Service(item="Update")
    if isinstance(section_shelly_status.get('uptime'), int):
        yield Service(item='Uptime')



def check_shelly(item, section_shelly_settings, section_shelly_status):
    if item=='Device':
        yield Result(state=State.OK, summary=f"Type: %s" % section_shelly_settings.get('device').get('type'))
        yield Result(state=State.OK, summary=f"Name: %s" % section_shelly_settings.get('device').get('hostname'))
        yield Result(state=State.OK, summary=f"Discoverable: %s" % section_shelly_settings.get('discoverable'))
        yield Result(state=State.OK, summary=f"Report period: %smin" % section_shelly_settings.get('device').get('report_period'))
    elif item=='WiFi':
        if section_shelly_settings.get('wifi_ap', {}).get('enabled', None):
            yield Result(state=State.OK, summary=f"WiFi type: AP")
            yield Result(state=State.OK, summary=f"SSID: %s" % section_shelly_settings.get('wifi_ap', {}).get('ssid', None))
        elif section_shelly_settings.get('wifi_sta', {}).get('enabled', None):
            yield Result(state=State.OK, summary=f"WiFi type: client")
            yield Result(state=State.OK, summary=f"SSID: %s" % section_shelly_status.get('wifi_sta', {}).get('ssid', None))
            yield Result(state=State.OK, summary=f"RSSI: %s" % section_shelly_status.get('wifi_sta', {}).get('rssi', None))
            yield Metric("RSSI", section_shelly_status.get('wifi_sta', {}).get('rssi', None))
    elif item=='MQTT':
        if section_shelly_settings.get('mqtt', {}).get('enable', None):
            yield Result(state=State.OK, summary=f"MQTT: enabled")
            yield Result(state=State.OK, summary=f"Server: %s" % section_shelly_settings.get('mqtt', {}).get('server', None))
            yield Result(state=State.OK, summary=f"Update period: %ss" % section_shelly_settings.get('mqtt', {}).get('update_period', None))
            if not section_shelly_status.get('mqtt', {}).get('connected') and section_shelly_settings.get('mqtt', {}).get('enable'):
                yield Result(state=State.WARN, summary=f"Connected: %s" % section_shelly_status.get('mqtt', {}).get('connected', None))
            else:
                yield Result(state=State.OK, summary=f"Connected: %s" % section_shelly_status.get('mqtt', {}).get('connected', None))
        else:
            yield Result(state=State.OK, summary=f"MQTT: disabled")
    elif item=='CoIoT':
        if section_shelly_settings.get('coiot', {}).get('enabled', None):
            yield Result(state=State.OK, summary=f"CoIoT: enabled")
        else:
            yield Result(state=State.OK, summary=f"CoIoT: disabled")
        yield Result(state=State.OK, summary=f"Update period: %ss" % section_shelly_settings.get('coiot', {}).get('update_period', None))
        if section_shelly_settings.get('coiot', {}).get('peer', None)=="":
            yield Result(state=State.OK, summary=f"Peer: --")
        else:
            yield Result(state=State.OK, summary=f"Peer: %s" % section_shelly_settings.get('coiot', {}).get('peer', None))
    elif item=='Cloud':
        if section_shelly_settings.get('cloud', {}).get('enabled', None):
            yield Result(state=State.OK, summary=f"Cloud: enabled")
            if section_shelly_status.get('cloud', {}).get('connected'):
                yield Result(state=State.OK, summary=f"Connected: %s" % section_shelly_status.get('cloud', {}).get('connected'))
            else:
                yield Result(state=State.WARN, summary=f"Connected: %s" % section_shelly_status.get('cloud', {}).get('connected'))
        else:
            yield Result(state=State.OK, summary=f"Cloud: disabled")
    elif item=='Update':
        update=section_shelly_status.get('update',{})
        yield Result(state=State.OK, summary=f"Status: %s" % update.get('status', None))
        if not update.get('has_update', None):
            yield Result(state=State.OK, summary=f"Has update: %s" % update.get('has_update', None))
            yield Result(state=State.OK, summary=f"Version: %s" % update.get('old_version', None))
        elif update.get('has_update', None):
            yield Result(state=State.WARN, summary=f"Has update: %s" % update.get('has_update', None))
            yield Result(state=State.OK, summary=f"Version: %s" % update.get('old_version', None))
            yield Result(state=State.OK, summary=f"New version: %s" % update.get('new_version', None))
    elif item=='Uptime':
            yield Result(state=State.OK, summary=f"Uptime: %s" % get_age_human_readable(section_shelly_status.get('uptime')))
            yield Metric( "uptime", section_shelly_status.get('uptime'))


def discover_shellypower(section_shelly_settings, section_shelly_status):
    emeters = section_shelly_status.get('emeters', {})
    for name, emeter in enumerate(emeters):
        yield Service(item='L%s' % str(name+1))
    if section_shelly_status.get('total_power', None)!=None:
        yield Service(item="total")

def check_shellypower(item, params, section_shelly_settings, section_shelly_status):
    if params is None:
        params = {}

    if item=="total":
        energy=0.0
        energy_returned=0.0
        emeters = section_shelly_status.get('emeters', {})
        for name, emeter in enumerate(emeters):
            energy=energy+emeter.get('total', 0.0)
            energy_returned=energy_returned+emeter.get('total_returned', 0.0)

        yield from check_levels(
            value=section_shelly_status.get('total_power'),
            levels_upper=params.get("power_level_upper"),
            levels_lower=params.get("power_level_lower"),
            metric_name="power",
            label="Power",
            render_func=lambda v: f"{v:.2f}W",  # pylint: disable=cell-var-from-loop,
            boundaries=(0, 30000),
            notice_only=False,
        )

        yield from check_levels(
            value=energy,
            metric_name="energy",
            label="Energy",
            render_func=lambda v: f"{v/1000:.3f}kWh",  # pylint: disable=cell-var-from-loop,
        )

        if energy_returned>200:
            notice_only=False
        else:
            notice_only=True

        yield from check_levels(
            value=energy_returned,
            metric_name="shelly_energy_returned",
            label="Energy returned",
            render_func=lambda v: f"{v/1000:.3f}kWh",  # pylint: disable=cell-var-from-loop,
            notice_only=notice_only,
        )

    else:
        emeters = section_shelly_status.get('emeters', {})
        for name, emeter in enumerate(emeters):
            if item=="L%s" % str(name+1):
                if emeter.get('is_valid'):
                    yield Result(state=State.OK, notice=f"Valid: %s" % emeter.get('is_valid', None))
                else:
                    yield Result(state=State.WARN, notice=f"Valid: %s" % emeter.get('is_valid', None))

                yield from check_levels(
                    value=emeter.get('power'),
                    levels_upper=params.get("power_level_upper"),
                    levels_lower=params.get("power_level_lower"),
                    metric_name="power",
                    label=_("Power"),
                    render_func=lambda v: f"{v:.2f}W",  # pylint: disable=cell-var-from-loop,
                    boundaries=(0, 10000),
                    notice_only=False,
                )

                yield from check_levels(
                    value=emeter.get('current'),
                    levels_upper=params.get("current_level_upper"),
                    levels_lower=params.get("current_level_lower"),
                    metric_name="current",
                    label=_("Current"),
                    render_func=lambda v: f"{v:.2f}A",  # pylint: disable=cell-var-from-loop,
                    boundaries=(0, 16),
                    notice_only=False,
                )

                yield from check_levels(
                    value=emeter.get('voltage'),
                    levels_upper=params.get("voltage_level_upper"),
                    levels_lower=params.get("voltage_level_lower"),
                    metric_name="voltage",
                    label=_("Voltage"),
                    render_func=lambda v: f"{v:.2f}V",  # pylint: disable=cell-var-from-loop,
                    boundaries=(190, 250),
                )

                yield from check_levels(
                    value=emeter.get('pf'),
                    metric_name="shelly_pf",
                    label=_("Power-Factor"),
                    render_func=lambda v: f"{v:.2f}",  # pylint: disable=cell-var-from-loop,
                    boundaries=(0, 1),
                )

                yield from check_levels(
                    value=emeter.get('total'),
                    metric_name="energy",
                    label=_("Energy"),
                    render_func=lambda v: f"{v/1000:.3f}kWh",  # pylint: disable=cell-var-from-loop,
                )


                if emeter.get('total_returned')>100:
                    notice_only=False
                else:
                    notice_only=True

                yield from check_levels(
                    value=emeter.get('total_returned'),
                    metric_name="shelly_energy_returned",
                    label=_("Energy returned"),
                    render_func=lambda v: f"{v/1000:.3f}kWh",  # pylint: disable=cell-var-from-loop,
                    notice_only=notice_only,
                )


register.agent_section(
    name="shelly_device",
    parse_function=parse_shelly,
)

register.agent_section(
    name="shelly_settings",
    parse_function=parse_shelly,
)

register.agent_section(
    name="shelly_status",
    parse_function=parse_shelly,
)


register.check_plugin(
    name = "shelly_device",
    service_name = "SHELLY %s",
    sections=["shelly_settings", "shelly_status"],
    discovery_function = discover_shelly,
    check_function = check_shelly,
)


register.check_plugin(
    name = "shelly_power",
    service_name = "SHELLY Power_%s",
    sections=["shelly_settings", "shelly_status"],
    discovery_function = discover_shellypower,
    check_function = check_shellypower,
    check_default_parameters = {
        'voltage_level_lower': (207, 210),
        'voltage_level_upper': (250, 253),
        'current_level_lower': (None, None),
        'current_level_upper': (None, None),
        'power_level_lower': (None, None),
        'power_level_upper': (None, None),
    },
    check_ruleset_name = "shelly_power",
)
